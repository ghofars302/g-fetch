const http = require('http');
const https = require('https');

URL = URL ? URL : require('url').URL;

const UserAgent = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36'
];

const requestResults = async (rawData) => {
    return {
        json: async () => {
            if (!rawData) throw new Error('Invalid response body, or it already used.');
        
            try {
                const result = typeof rawData === 'string' ? JSON.parse(rawData) : JSON.parse(rawData.toString());
        
                rawData = undefined;
        
                return result;
            } catch (error) {
                throw error;
            }
        },
        text: async () => {
            if (!rawData) throw new Error('Invalid response body, or it already used.');
        
            const result = Buffer.isBuffer(rawData) ? rawData.toString() : rawData;
        
            rawData = undefined;
        
            return result;
        },
        buffer: async () => {
            if (!rawData) throw new Error('Invalid response body, or it already used.');
        
            const result = typeof rawData === 'string' ? Buffer.from(rawData) : rawData;
        
            rawData = undefined;
        
            return result;
        }
    }
}

module.exports = (url, options) => {
    const { request } = url.startsWith('https://') ? https : http;

    const QueryOptions = {
        method: ['get', 'post', 'delete', 'head', 'put', 'connect', 'options', 'trace'].includes(options.method.toLowerCase()) ? options.method.toUpperCase() : 'GET',
        headers: options.headers ? options.headers : {
            'User-Agent': options['User-Agent'] ? options['User-Agent'] : UserAgent[Math.floor(Math.random*UserAgent.length)]
        },
    }

    url = new URL(url);
    if (options.qs) for (const str of Object.keys(options.qs)) url.searchParams.append(options.qs[str]);

    return new Promise((resolve, reject) => {
        request(url, QueryOptions, (res) => {
            let data = '';

            res.on('data', chunk => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(requestResult(data));
            })
        }).catch(error => reject(error));
    })
}